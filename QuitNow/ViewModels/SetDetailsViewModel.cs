﻿using MvvmCross.Core.ViewModels;
using QuitNow.Models;
using System;
using System.Text.RegularExpressions;

namespace QuitNow.ViewModels
{
    public sealed class SetDetailsViewModel : MvxViewModel
    {
        private readonly SetDetailsModel _model;
        public DateTime QuitDate
        {
            get { return _model.QuitDate; }
            set
            {
                _model.QuitDate = value;
                RaisePropertyChanged(() => QuitDate);
            }
        }
        public decimal CostPerPack
        {
            get { return _model.CostPerPack; }
            set
            {
                var regex = new Regex("[\\d]{0,4}([.,][\\d]{1,2})?");
                if (regex.IsMatch(value.ToString()) && value > 0)
                {
                    _model.CostPerPack = value;
                    RaisePropertyChanged(() => CostPerPack);
                }
            }
        }
        public decimal PacksPerDay
        {
            get { return _model.PacksPerDay; }
            set
            {
                var regex = new Regex("[\\d]{0,4}([.,][\\d]{1,2})?");
                if (regex.IsMatch(value.ToString()) && value > 0)
                {
                    _model.PacksPerDay = value;
                    RaisePropertyChanged(() => PacksPerDay);

                }
            }
        }

        public string DaysNotSmokedDisplay
        {
            get
            {
                var dateDifferenceHours = (DateTime.Now - QuitDate).TotalHours;
                if (dateDifferenceHours > 0)
                {
                    var multiplier = CostPerPack * PacksPerDay;
                    var days = dateDifferenceHours / 24;
                    return days.ToString("N1") + " Days Not Smoked";
                }

                return "0 Days Not Smoked";
            }
        }

        public string MoneySavedDisplay
        {
            get
            {
                var dateDifferenceHours = (DateTime.Now - QuitDate).TotalHours;
                if(dateDifferenceHours > 0)
                {
                    var multiplier = CostPerPack * PacksPerDay;
                    var days = dateDifferenceHours / 24;
                    var hours = dateDifferenceHours % 24;
                    var savings = (dateDifferenceHours * (double)multiplier) / 24;
                    return string.Format("${0:#.00}", savings) + " Saved";
                }

                return "$0.00 Saved";
            }
        }
        public IMvxCommand SaveDetailsCommand { get; }
        public SetDetailsViewModel(SetDetailsModel model)
        {
            this._model = model;
            SaveDetailsCommand = new MvxCommand(OnSaveDetailsCommand);
        }
        private void OnSaveDetailsCommand()
        {
            _model?.SaveData();
        }
    }
}
