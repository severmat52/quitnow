﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuitNow.Models
{
    public sealed class SetDetailsModel
    {
        public DateTime QuitDate { get; set; } = DateTime.Now;
        public decimal CostPerPack { get; set; } = 8.00m;
        public decimal PacksPerDay { get; set; } = 1m;

        public static SetDetailsModel RestoreFromProperties()
        {
            var model = new SetDetailsModel();

            try
            {
                //QuitDate
                if (App.Current.Properties.ContainsKey("QuitDate"))
                    model.QuitDate = DateTime.Parse(App.Current.Properties["QuitDate"]?.ToString());
                if (App.Current.Properties.ContainsKey("CostsPerPack"))
                    model.CostPerPack = decimal.Parse(App.Current.Properties["CostsPerPack"].ToString());
                if (App.Current.Properties.ContainsKey("PacksPerDay"))
                    model.PacksPerDay = decimal.Parse(App.Current.Properties["PacksPerDay"]?.ToString());

            }
            catch (Exception)
            {
                //ignore
            }

            return model;
        }

        public void SaveData()
        {
            if (App.Current.Properties.ContainsKey("QuitDate"))
                App.Current.Properties["QuitDate"] = this.QuitDate;
            else
                App.Current.Properties.Add("QuitDate", this.QuitDate);
            if (App.Current.Properties.ContainsKey("CostPerPack"))
                App.Current.Properties["CostPerPack"] = this.CostPerPack;
            else
                App.Current.Properties.Add("CostPerPack", CostPerPack);
            if (App.Current.Properties.ContainsKey("PacksPerDay"))
                App.Current.Properties["PacksPerDay"] = this.PacksPerDay;
            else
                App.Current.Properties.Add("PacksPerDay", PacksPerDay);
        }
    }
}
