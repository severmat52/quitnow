﻿using QuitNow.ViewModels;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace QuitNow.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SetDetailsView : ContentPage
	{
		public SetDetailsView ()
		{
			InitializeComponent ();
            this.BindingContext = new SetDetailsViewModel(new Models.SetDetailsModel());
            if(Device.RuntimePlatform == Device.iOS){
                dateIcon.Source = ImageSource.FromResource("QuitNow.iOS.Resources.icons8-calendar-filled-50.png");
                cigaretteIcon.Source = ImageSource.FromResource("QuitNow.iOS.Resources.icons8-smoking-50.png");
                moneyIcon.Source = ImageSource.FromResource("QuitNow.iOS.Resources.icons8-money-50.png");
            }
        }
	}
}