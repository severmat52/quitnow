﻿using QuitNow.Views;
using System;

using Xamarin.Forms;

namespace QuitNow
{
    public class MainPage : TabbedPage
    {
        public MainPage()
        {
            Page itemsPage, aboutPage = null;

            switch (Device.RuntimePlatform)
            {
                case Device.iOS:
                    itemsPage = new NavigationPage(new SetDetailsView())
                    {
                        Title = "Set Details"
                    };

                    aboutPage = new NavigationPage(new AboutPage())
                    {
                        Title = "View Progress"
                    };
                    itemsPage.Icon = "icons8-save-filled-50.png";
                    aboutPage.Icon = "tab_about.png";
                    break;
                default:
                    itemsPage = new SetDetailsView()
                    {
                        Title = "Set Details"
                    };

                    aboutPage = new AboutPage()
                    {
                        Title = "View Progress"
                    };
                    break;
            }

            Children.Add(itemsPage);
            Children.Add(aboutPage);

            Title = Children[0].Title;
        }

        protected override void OnCurrentPageChanged()
        {
            base.OnCurrentPageChanged();
            Title = CurrentPage?.Title ?? string.Empty;
        }
    }
}
