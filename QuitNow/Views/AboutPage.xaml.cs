﻿using QuitNow.Models;
using QuitNow.ViewModels;
using Xamarin.Forms;

namespace QuitNow
{
    public partial class AboutPage : ContentPage
    {
        public AboutPage()
        {
            InitializeComponent();
            this.BindingContext = new SetDetailsViewModel(SetDetailsModel.RestoreFromProperties());
        }

        protected override void OnAppearing()
        {
            this.BindingContext = new SetDetailsViewModel(SetDetailsModel.RestoreFromProperties());
        }
    }
}
